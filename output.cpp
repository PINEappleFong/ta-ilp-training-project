#include <ctime>
#include <cstdio>
#include <iostream>
#include <string.h>
#include <string>
#include <stdio.h>
#include <vector>
#include <time.h>
#include <fstream>
#include <deque>
#include <utility>
#include <map>

using namespace std;
class GuideParser
{
public:
	GuideParser()
	{
	}

	struct rectangle
	{
		unsigned int lbx, lby, rtx, rty, layer;

		rectangle(unsigned int _lbx, unsigned int _lby, unsigned int _rtx, unsigned int _rty, unsigned int _layer)
		{
			lbx = _lbx;
			lby = _lby;
			rtx = _rtx;
			rty = _rty;
			layer = _layer;
		}
	};

	vector<vector<rectangle>> net_guide;
	vector<vector<rectangle>> net_iroute;
	map<string, int> NetTable;

	void guide_fileInput(istream &file) // file for the input.guide
	{
		int counter = 0;

		string line_guide;
		while (!file.eof())
		{
			counter++;

			getline(file, line_guide); // net name
			auto design_net = NetTable.find(line_guide);

			if (design_net == NetTable.end())
			{
				// not found, add to map
				NetTable.emplace(line_guide, counter);
			}

			vector<rectangle> temp_guide;

			if (line_guide.size() > 0 && line_guide.at(0) != '(')
			{

				getline(file, line_guide); // (
				while (line_guide.size() > 0 && line_guide.at(0) != ')')
				{
					if (line_guide.find(" ") != string::npos)
					{
						int M_pos = line_guide.find(' ');

						int index_start_coor;
						unsigned int _x[2], _y[2], layer_number;

						string layer_num_s;
						layer_num_s.clear();
						for (int ind = 0; ind < 4; ind++)
						{
							for (index_start_coor = 0; line_guide.at(index_start_coor) != ' ';)
							{
								layer_num_s.push_back(line_guide.at(index_start_coor));
								line_guide.erase(line_guide.begin());
							}

							line_guide.erase(line_guide.begin());

							if (ind == 0)
								_x[0] = atoi(layer_num_s.c_str());
							else if (ind == 1)
								_x[1] = atoi(layer_num_s.c_str());
							else if (ind == 2)
								_y[0] = atoi(layer_num_s.c_str());
							else if (ind == 3)
								_y[1] = atoi(layer_num_s.c_str());

							layer_num_s.clear();
						} // for

						layer_num_s.push_back(line_guide.at(line_guide.size() - 1));
						layer_number = atoi(layer_num_s.c_str());

						//cout << ">> " << _x[0] << " " << _x[1] << " " << _y[0] << " " << _y[1] << " " << layer_number << endl;
						rectangle temp_rect = rectangle(_x[0], _x[1], _y[0], _y[1], layer_number);
						temp_guide.push_back(temp_rect);
					}

					getline(file, line_guide);

				} // while

				//net_guide.push_back(temp_guide);

			} // if

			net_guide.push_back(temp_guide);
			temp_guide.clear();
		}

		return;
	} // read input()

	void iroute_fileInput(istream &file)
	{
		int counter = 0;

		string line_guide;
		while (!file.eof())
		{
			counter++;

			int guide_index = -1;

			getline(file, line_guide); // net name
			auto design_net = NetTable.find(line_guide);

			if (design_net == NetTable.end())
			{
				// not found, add to map
				printf("ERROR::(%s) not found in guide file.\n");
			}
			else
			{
				guide_index = NetTable.find(line_guide)->second;
			}

			vector<rectangle> temp_iroute;

			if (line_guide.size() > 0 && line_guide.at(0) != '(')
			{

				getline(file, line_guide);
				while (line_guide.size() > 0 && line_guide.at(0) != ')')
				{
					if (line_guide.find(" ") != string::npos)
					{
						int M_pos = line_guide.find(' ');

						int index_start_coor;
						unsigned int _x[2], _y[2], layer_number;

						string layer_num_s;
						layer_num_s.clear();
						for (int ind = 0; ind < 4; ind++)
						{
							for (index_start_coor = 0; line_guide.at(index_start_coor) != ' ';)
							{
								layer_num_s.push_back(line_guide.at(index_start_coor));
								line_guide.erase(line_guide.begin());
							}

							line_guide.erase(line_guide.begin());

							if (ind == 0)
								_x[0] = atoi(layer_num_s.c_str());
							else if (ind == 1)
								_x[1] = atoi(layer_num_s.c_str());
							else if (ind == 2)
								_y[0] = atoi(layer_num_s.c_str());
							else if (ind == 3)
								_y[1] = atoi(layer_num_s.c_str());

							layer_num_s.clear();
						} // for

						layer_num_s.push_back(line_guide.at(line_guide.size() - 1));
						layer_number = atoi(layer_num_s.c_str());

						//cout << ">> " << _x[0] << " " << _x[1] << " " << _y[0] << " " << _y[1] << " " << layer_number << endl;
						rectangle temp_rect = rectangle(_x[0], _x[1], _y[0], _y[1], layer_number);
						temp_iroute.push_back(temp_rect);
					}

					getline(file, line_guide);

				} // while

				//net_iroute.push_back(temp_iroute);

			} // if

			net_iroute.push_back(temp_iroute);
			temp_iroute.clear();
		}

		return;
	} // read input()
};


int main(int argc, char *argv[])
{
	string Iroute_File, Guide_File, Output_File, GDT_TEMP;

	if (argc != 7)
	{
		cout << "usage -guide [guide_file] -iroute [ilp-output_file] -output <output_file_name>\n";
		exit(0);
	}

	// parse input
	for (int argc_index = 1; argc_index < argc; argc_index++)
	{
		if (strcmp(argv[argc_index], "-guide") == 0)
		{
			Guide_File = argv[argc_index + 1];
		}
		else if (strcmp(argv[argc_index], "-iroute") == 0)
		{
			Iroute_File = argv[argc_index + 1];
		}
		else if (strcmp(argv[argc_index], "-output") == 0)
		{
			Output_File = argv[argc_index + 1];
			GDT_TEMP = Output_File + ".gdt";
		}

		argc_index++;
	}

	// parse end

	//printf("%s\n", argv[1]);
	//printf("%s\n", argv[2]);
	char leffile[100];
	char deffile[100];

	GuideParser GP;
	printf("Transfer...\n");
	std::filebuf guide_fb;
	guide_fb.open(Guide_File , std::ios::in);
	istream guide_is(&guide_fb);
	GP.guide_fileInput(guide_is);
	guide_fb.close();

	std::filebuf iroute_fb;
	iroute_fb.open(Iroute_File, std::ios::in);
	istream iroute_is(&iroute_fb);
	GP.iroute_fileInput(iroute_is);

	iroute_fb.close();

	FILE *ofile = fopen(GDT_TEMP.c_str(), "w");

	// get current time, and output to a string
	time_t rawtime;
	struct tm *timeinfo = nullptr;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	char timeseq[25];
	// tm_year is the years counted from 1900 A.D.

	// output layout info
	sprintf(timeseq, "%d-%02d-%02d %02d:%02d:%02d", timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

	fputs("gds2{600\n", ofile);
	fprintf(ofile, "m=%s a=%s\n", timeseq, timeseq);
	fputs("lib 'NCTU' 0.1 1e-7\n", ofile);

	fprintf(ofile, "cell{c=%s m=%s '%s'\n", timeseq, timeseq, "TrackAssignment Project");

	for (int net_index = 0; net_index < GP.net_guide.size(); net_index++)
	{
		int net_gds_layer = net_index + 1;
		//printf("gds-layer(%d)\n", net_gds_layer);
		for (int current_index = 0; current_index < GP.net_guide.at(net_index).size(); current_index++)
		{
			GuideParser::rectangle &rect_temp = GP.net_guide.at(net_index).at(current_index);
			//printf("b{%d dt%d xy(%d %d %d %d %d %d %d %d)}\n", net_gds_layer, 0, rect_temp.lbx, rect_temp.lby, rect_temp.rtx, rect_temp.lby, rect_temp.rtx, rect_temp.rty, rect_temp.lbx, rect_temp.rty);
			fprintf(ofile, "b{%d dt%d xy(%d %d %d %d %d %d %d %d)}\n", net_gds_layer, 0, rect_temp.lbx, rect_temp.lby, rect_temp.rtx, rect_temp.lby, rect_temp.rtx, rect_temp.rty, rect_temp.lbx, rect_temp.rty);
		}
	}
	for (int net_index = 0; net_index < GP.net_iroute.size(); net_index++)
	{
		int net_gds_layer = net_index + 1;
		for (int current_index = 0; current_index < GP.net_iroute.at(net_index).size(); current_index++)
		{
			GuideParser::rectangle &rect_temp = GP.net_iroute.at(net_index).at(current_index);
			fprintf(ofile, "b{%d dt%d xy(%d %d %d %d %d %d %d %d)}\n", net_gds_layer, 1, rect_temp.lbx, rect_temp.lby, rect_temp.rtx, rect_temp.lby, rect_temp.rtx, rect_temp.rty, rect_temp.lbx, rect_temp.rty);
		}
	}
	// gdt end
	fputs("}\n}\n", ofile);
	
	fclose(ofile);

	// generate command
	char convert_cmd[512];
	sprintf(convert_cmd, "GDT-4.0.4/gdt2gds %s %s.gds", GDT_TEMP.c_str(), Output_File.c_str());
	const int cmd_status = system(convert_cmd);
	
	return 0;
}
