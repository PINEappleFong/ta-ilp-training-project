#include <iostream>
#include <string>
#include <stdio.h>
#include <vector>
#include <time.h>
#include <fstream>
#include <deque>
#include <utility>
#include <map>

using namespace std;
class GuideParser
{
  public:
    GuideParser()
    {
        
    }

    struct rectangle
    {
        unsigned int lbx, lby, rtx, rty, layer;

        rectangle(unsigned int _lbx, unsigned int _lby, unsigned int _rtx, unsigned int _rty, unsigned int _layer)
        {
            lbx = _lbx;
            lby = _lby;
            rtx = _rtx;
            rty = _rty;
            layer = _layer;
        }
    };

    vector<vector<rectangle>> net_guide;
    vector<vector<rectangle>> net_iroute;
    map<string, int> NetTable;


    void guide_fileInput(istream &file) // file for the input.guide
    {
        int counter = 0;      
        
        string line_guide;
        while (!file.eof())
        {
            counter++;

            getline(file, line_guide); // net name
            auto design_net = NetTable.find(line_guide);

            if (design_net == NetTable.end())
            {
                // not found, add to map
                NetTable.emplace(line_guide, counter);
            }

            vector<rectangle> temp_guide;

            if (line_guide.size() > 0 && line_guide.at(0) != '(')
            {
                
                getline(file, line_guide); // (
                while (line_guide.size() > 0 && line_guide.at(0) != ')')
                {
                    if (line_guide.find(" ") != string::npos)
                    {
                        int M_pos = line_guide.find(' ');

                        int index_start_coor;
                        unsigned int _x[2], _y[2], layer_number;

                        string layer_num_s;
                        layer_num_s.clear();
                        for (int ind = 0; ind < 4; ind++)
                        {
                            for (index_start_coor = 0; line_guide.at(index_start_coor) != ' ';)
                            {
                                layer_num_s.push_back(line_guide.at(index_start_coor));
                                line_guide.erase(line_guide.begin());
                            }

                            line_guide.erase(line_guide.begin());

                            if (ind == 0)
                                _x[0] = atoi(layer_num_s.c_str());
                            else if (ind == 1)
                                _x[1] = atoi(layer_num_s.c_str());
                            else if (ind == 2)
                                _y[0] = atoi(layer_num_s.c_str());
                            else if (ind == 3)
                                _y[1] = atoi(layer_num_s.c_str());

                            layer_num_s.clear();
                        } // for

                        layer_num_s.push_back(line_guide.at(line_guide.size() - 1));
                        layer_number = atoi(layer_num_s.c_str());

                        //cout << ">> " << _x[0] << " " << _x[1] << " " << _y[0] << " " << _y[1] << " " << layer_number << endl;
                        rectangle temp_rect = rectangle(_x[0], _x[1], _y[0], _y[1], layer_number);
                        temp_guide.push_back(temp_rect);
                    }

                    getline(file, line_guide);

                } // while

                //net_guide.push_back(temp_guide);

            } // if

            net_guide.push_back(temp_guide);
            temp_guide.clear();
        }

        printf(">>>>> %d\n",counter);
        return;
    } // read input()

    void iroute_fileInput(istream &file)
    {
        int counter = 0;

        string line_guide;
        while (!file.eof())
        {
            counter++;

            int guide_index = -1;

            getline(file, line_guide); // net name
            auto design_net = NetTable.find(line_guide);

            if (design_net == NetTable.end())
            {
                // not found, add to map
                printf("ERROR::(%s) not found in guide file.\n");
            }
            else
            {
                guide_index = NetTable.find(line_guide)->second;
            }
            
            vector<rectangle> temp_iroute;

            if (line_guide.size() > 0 && line_guide.at(0) != '(')
            {

                getline(file, line_guide);
                while (line_guide.size() > 0 && line_guide.at(0) != ')')
                {
                    if (line_guide.find(" ") != string::npos)
                    {
                        int M_pos = line_guide.find(' ');

                        int index_start_coor;
                        unsigned int _x[2], _y[2], layer_number;

                        string layer_num_s;
                        layer_num_s.clear();
                        for (int ind = 0; ind < 4; ind++)
                        {
                            for (index_start_coor = 0; line_guide.at(index_start_coor) != ' ';)
                            {
                                layer_num_s.push_back(line_guide.at(index_start_coor));
                                line_guide.erase(line_guide.begin());
                            }

                            line_guide.erase(line_guide.begin());

                            if (ind == 0)
                                _x[0] = atoi(layer_num_s.c_str());
                            else if (ind == 1)
                                _x[1] = atoi(layer_num_s.c_str());
                            else if (ind == 2)
                                _y[0] = atoi(layer_num_s.c_str());
                            else if (ind == 3)
                                _y[1] = atoi(layer_num_s.c_str());

                            layer_num_s.clear();
                        } // for

                        layer_num_s.push_back(line_guide.at(line_guide.size() - 1));
                        layer_number = atoi(layer_num_s.c_str());

                        cout << ">> " << _x[0] << " " << _x[1] << " " << _y[0] << " " << _y[1] << " " << layer_number << endl;
                        rectangle temp_rect = rectangle(_x[0], _x[1], _y[0], _y[1], layer_number);
                        temp_iroute.push_back(temp_rect);
                    }

                    getline(file, line_guide);

                } // while

                //net_iroute.push_back(temp_iroute);

            } // if

            net_iroute.push_back(temp_iroute);
            temp_iroute.clear();
        }

        return;
    } // read input()
};
