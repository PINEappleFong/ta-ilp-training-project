CXX		=	g++ -std=c++1y -static -O3
EXE		=	answer2gds
SRC_DIR	=	.
OBJ_DIR	=	.


all: $(EXE)


$(EXE): $(OBJ_DIR)/output.o
	$(CXX) $^ -o $@ -lrt

$(OBJ_DIR)/output.o: output.cpp | obj_dir
	$(CXX) -c $< -o $@
 
obj_dir:
	mkdir -p $(OBJ_DIR)


clean:
	rm output.o $(EXE)
	cd ..
